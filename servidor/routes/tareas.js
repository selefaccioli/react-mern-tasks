const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const tareaController = require('../controllers/tareaController');
const auth = require('../middleware/auth');

//api/tareas
router.post('/',
    auth,
    [
        check('nombre', 'El nombre de la tarea es obligatorio').not().isEmpty()
    ],
    tareaController.crearTarear
);

router.get('/',
    auth,
    tareaController.tareasPorProyecto
);

router.put('/:id',
    auth,
    tareaController.actualizarTarea
);

router.delete('/:id',
    auth,
    tareaController.eliminarTarea
);

module.exports = router;

