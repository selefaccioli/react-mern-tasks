const Tarea = require('../models/Tarea');
const Proyecto = require('../models/Proyecto');
const { validationResult } = require('express-validator');

exports.crearTarear = async (req, res) => {
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() })
    }

    try {
       const { proyecto } = req.body;

       const proyectoExistente = await Proyecto.findById(proyecto);

       if(!proyectoExistente) {
           res.status(404).json({ msg: 'Proyecto no encontrado' });
       }

       if(proyectoExistente.creador.toString() !== req.usuario.id){
           return res.status(401).json({ msg: 'Usuario no autorizado' });
       }

       const tarea = new Tarea(req.body);
       await tarea.save();
       res.json({ tarea });

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

exports.tareasPorProyecto = async (req, res) => {
    try {
        const { proyecto } = req.query;

        const proyectoExistente = await Proyecto.findById(proyecto);

        if(!proyectoExistente) {
            res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        if(proyectoExistente.creador.toString() !== req.usuario.id){
            return res.status(401).json({ msg: 'Usuario no autorizado' });
        }

        const tareas = await Tarea.find({ proyecto });
        res.json({ tareas });
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
    
}

exports.actualizarTarea = async (req, res) => {
    const errores = validationResult(req);
    if(!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() })
    }

    try {

        const { proyecto, nombre, estado } = req.body;

        let tarea = Tarea.findById(req.params.id);

        if(!tarea) {
            return res.status(404).send('Tarea no encontrada');
        }
        
        const proyectoExistente = await Proyecto.findById(proyecto);

        if(!proyectoExistente) {
            res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        if(proyectoExistente.creador.toString() !== req.usuario.id){
           return res.status(401).json({ msg: 'Usuario no autorizado' });
        }
        
        const nuevaTarea = {};

        nuevaTarea.nombre = nombre;
        nuevaTarea.estado = estado;

        tarea = await Tarea.findOneAndUpdate({ _id: req.params.id }, nuevaTarea, { new: true });
        res.json(tarea);
    } catch(error) {
        console.log(error);
        res.status(500).send('Hubo un error');
    }

}

module.exports.eliminarTarea = async (req, res) => {
    try {
        const { proyecto } = req.query;

        let tarea = await Tarea.findById(req.params.id);

        if(!tarea) {
            return res.status(404).json('Tarea no encontrada');
        }

        const proyectoExistente = await Proyecto.findById(proyecto);

        if(!proyectoExistente) {
            res.status(404).json({ msg: 'Proyecto no encontrado' });
        }

        if(proyectoExistente.creador.toString() !== req.usuario.id){
           return res.status(401).json({ msg: 'Usuario no autorizado' });
        }

        tarea = await Tarea.findOneAndDelete({ _id: req.params.id });
        res.json({ msg: 'Tarea eliminada' });

    } catch(error) {
        console.log(error);
        res.status(500).send('Huebo un error')
    }
}