import React, {Fragment, useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';
import Tarea from './Tarea';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const ListadoTarea = () => {

  //Obtener el state del proyecto
  const proyectosContext = useContext(proyectoContext);
  const { proyecto, eliminarProyecto } = proyectosContext;

  //Obtener las tareas del proyecto
  const tareasContext = useContext(tareaContext);
  const { tareasProyecto } = tareasContext;

  //Si no hay proyecto seleccionado
  if(!proyecto) return <h2>Selecciona un proyecto</h2>

  //Array destructuring para extraer el proyecto actual
  const [proyectoActual] = proyecto;

  return ( 
    <Fragment>
      <h2>{ proyectoActual.nombre }</h2>

      <ul className="listado-tareas">
        { tareasProyecto.length === 0 
          ? (<li className="tarea"><p>No hay tareas</p></li>)

          : <TransitionGroup>
            {  tareasProyecto.map(tarea => (
              <CSSTransition
                key={tarea._id}
                timeout={200}
                classNames="tarea"
              >
                  <Tarea 
                    key={tarea._id}
                    tarea={tarea}
                  />
              </CSSTransition>
              ))
            }
          </TransitionGroup>
          
        }

        <button 
          type="button"
          className="btn btn-eliminar"
          onClick={() => eliminarProyecto(proyectoActual._id)}
        >Eliminar Proyecto &times;</button>
      </ul>
    </Fragment>
   );
}
 
export default ListadoTarea;