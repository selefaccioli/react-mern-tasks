import React, { useState, useContext, useEffect } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

const FormTarea = () => {
  //Extraer si hay un proyecto activo
  const proyectosContext = useContext(proyectoContext);
  const { proyecto } = proyectosContext;

  //Obtener las tareas del proyecto
  const tareasContext = useContext(tareaContext);
  const { errorTarea, tareaSeleccionada, agregarTarea, validarTarea, obtenerTareas, actualizarTarea } = tareasContext;

  //Effect que detecta si hay una tarea seleccionada
  useEffect(() => {
    if(tareaSeleccionada !== null) {
      guardarTarea(tareaSeleccionada)
    } else {
      guardarTarea({
        nombre: ''
      })
    }
  }, [tareaSeleccionada]);
  
  const [tarea, guardarTarea] = useState({
    nombre: ''
  });

  const { nombre } = tarea;


  //Si no hay proyecto seleccionado
  if(!proyecto) return null;

  //Array destructuring para extraer el proyecto actual
  const [proyectoActual] = proyecto;

  const onChange = e => {
    guardarTarea({
      ...tarea,
      [e.target.name] : e.target.value
    })
  }

  const onSubmit = e => {
    e.preventDefault();

    //validar
    if(nombre.trim() === '') {
      validarTarea();
      return;
    }

    if(tareaSeleccionada === null) {
      //agregar una nueva tarea al state de tareas
      tarea.proyecto = proyectoActual._id;
      agregarTarea(tarea);
    } else {
      actualizarTarea(tarea);
    }

    //Obtener y filtrar las tareas del proyecto
    obtenerTareas(proyectoActual._id);

    //reiniciar el form
    guardarTarea({
      nombre: ''
    })
  }

  return ( 
      <div className="formulario">
      <form
        onSubmit={onSubmit}
      >
        <div className="contenedor-input">

          <input
            type="text"
            className="input-text"
            placeholder="Nombre tarea..."
            name="nombre"
            value={nombre}
            onChange={onChange}
          />  

        </div>

        <div className="contenedor-input">

          <input 
            type="submit"
            className="btn btn-primario btn-submit btn-block"
            value={tareaSeleccionada ? "Editar Tarea" : "Agregar Tarea"}
          />

        </div>
      </form>

      {errorTarea ? <p className="mensaje error">El nombre de la tarea es obligatorio</p> : null}
    </div>
   );
}
 
export default FormTarea;