import React, {Fragment, useContext, useEffect} from 'react';
import Proyecto from './Proyecto';
import proyectoContext from '../../context/proyectos/proyectoContext';
import AlertaContext from '../../context/alertas/alertaContext';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const ListadoProyectos = () => {

  //Extraer proyectos de state inicial
  const proyectosContext = useContext(proyectoContext);
  const {proyectos, mensaje, obtenerProyectos} = proyectosContext;

  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;

  //Obtener proyectos cuando carga el componente
  useEffect(() => {
    if(mensaje) {
      mostrarAlerta(mensaje.msg, mensaje.categoria);
    }

    obtenerProyectos();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mensaje]);

  //Revisar si proyectos tiene contenido
  if(proyectos.length === 0) return <p>No hay proyectos, comienza creando uno.</p>;

  return ( 
    <Fragment>
      <ul className="listado-proyectos">

        {alerta ? (<div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>)  : null }

        <TransitionGroup>
          {proyectos.map(proyecto => (
            <CSSTransition
              key={proyecto._id}
              timeout={200}
              classNames="proyecto"
              >
                <Proyecto
                key={proyecto._id}
                proyecto={proyecto}
              />
                  
            </CSSTransition>
          ))}
        </TransitionGroup>
      </ul>
    </Fragment>
   );
}
 
export default ListadoProyectos;